<?php

namespace Drupal\customerror\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller for errors pages.
 */
final class CustomErrorController extends ControllerBase {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a CustomErrorController object.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   */
  public function __construct(FormBuilderInterface $formBuilder, ConfigFactoryInterface $configFactory, AccountProxyInterface $currentUser, RequestStack $requestStack) {
    $this->formBuilder = $formBuilder;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('request_stack')
    );
  }

  /**
   * The method that will get called, with the services above already available.
   *
   * @param int $code
   *   The code of error.
   */
  public function index($code) {
    if (!is_numeric($code)) {
      throw new AccessDeniedHttpException();
    }

    $theme = customerror_get_theme($code);

    /*
    This is probably a fragment to select theme. It doesn't work.
    Keeping it as a reminder.
    if (!empty($theme)) {
    global $custom_theme;
    $custom_theme = $theme;
    }
     */

    switch ($code) {
      case 403:
        $internal_path = substr($this->requestStack->getCurrentRequest()->getRequestUri(), strlen(base_path()));
        if ($internal_path) {
          $dest = parse_url($internal_path);
          if (isset($dest['query']['destination'])) {
            $_GET['destination'] = ($dest['query']['destination']);
          }
          else {
            $_GET['destination'] = $internal_path;
          }
        }
        else {
          $_GET['destination'] = $this->configFactory->get('system.site')->get('page.front');
        }
        $_SESSION['destination'] = $_GET['destination'];

      case 404:
      default:

        // Check if we should redirect.
        $destination   = $this->requestStack->getCurrentRequest()->getRequestUri();
        $redirect_list = $this->configFactory->get('customerror.settings')->get('redirect');
        $redirect_list = !empty($redirect_list) ? explode("\n", $redirect_list) : [];
        foreach ($redirect_list as $item) {
          [$src, $dst] = explode(' ', $item);

          if (isset($src) && isset($dst)) {
            $src = str_replace("/", "\\/", $src);
            $dst = str_replace("\r", "", $dst);

            // In case there are spaces in the URL, we escape them.
            $orig_dst = str_replace(" ", "%20", $destination);
            if (preg_match("/$src/", $orig_dst)) {
              /* drupal_goto($dst); */
              /* return new RedirectResponse(url($dst, array('absolute' => TRUE))); */
              $dst = ($dst == '<front>' ? Url::fromRoute($dst)->toString() : $dst);
              header('Location: ' . $dst, TRUE, 302);
              exit();
            }
          }
        }

        // Make sure that we sent an appropriate header.
        customerror_header($code);

        /* $content = t(\Drupal::config('customerror.settings')->get("{$code}.body")); */
        $content = $this->configFactory->get('customerror.settings')->get("{$code}.body");
        break;
    }

    $login_form = '';
    // If the user is not logged in, show the login form.
    if ($this->configFactory->get('customerror.settings')->get("{$code}.enable_login") && $this->currentUser->isAnonymous()) {

      // For reasons that are not clear to me, setting the form state redirect
      // here has no effect, logging in at this form puts us on a custom error
      // path looking like this: /customerror/403?destination=/admin/config
      // And that destination isn't going to do anything for us at that point.
      // $path = $this->getRequest()->getPathInfo();
      // $url = \Drupal\Core\Url::fromUserInput($path);
      $form_state = new FormState();
      // $form_state->setRedirectUrl($url);
      // Instead of the above code, we have to do the same thing in ... ah,
      // that's the deal, we have to do it in the submit handler or else it's
      // overridden, OK this makes sense now.
      $login_form = $this->formBuilder->getForm('Drupal\user\Form\UserLoginForm', $form_state);
    }

    return [
      '#theme' => 'customerror__' . $code,
      '#description' => $content,
      '#login_form' => $login_form,
      '#error_code' => $code,
    ];
  }

  /**
   * Title callback.
   *
   * @param int $code
   *   The code of error.
   *
   * @return string
   *   The title to page.
   */
  public function titleCallback($code) {
    /* $title = $this->t(\Drupal::config('customerror.settings')->get("{$code}.title")); */
    $title = $this->configFactory->get('customerror.settings')->get("{$code}.title");
    return $title;
  }

}
